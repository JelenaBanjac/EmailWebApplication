/**
 * 
 */
package beans.user;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User class for creating user instances
 * @author Jelena Banjac
 *
 */
@XmlRootElement
public class User implements Serializable {

	/**
	 * User class version
	 */
	private static final long serialVersionUID = -5152583641113739923L;
	/**
	 * User username
	 */
	private String username;
	/**
	 * User password
	 */
	private String password;
	/**
	 * User fist name
	 */
	private String firstName;
	/**
	 * User last name
	 */
	private String lastName;	
	/**
	 * Emails that are sent by user
	 */
	private ArrayList<String> sentEmails;
	/**
	 * Emails that user received
	 */
	private ArrayList<String> receivedEmails;
	
	/**
	 * Default user constructor
	 */
	public User() { 
		this.username = "";
		this.password = "";
		this.firstName = "";
		this.lastName = "";
		this.sentEmails = new ArrayList<String>();
		this.receivedEmails = new ArrayList<String>();
	}
	
	/**
	 * User constructor with the following parameter
	 * @param username
	 */
	public User(String username) {
		this.username = username;
		this.password = "";
		this.firstName = "";
		this.lastName = "";
		this.sentEmails = new ArrayList<String>();
		this.receivedEmails = new ArrayList<String>();
	}
	
	/**
	 * Copy constructor
	 * @param newUser
	 */
	public User(User newUser) {
		this.username = newUser.getUsername();
		this.password = newUser.getPassword();
		this.firstName = newUser.getFirstName();
		this.lastName = newUser.getLastName();
		
		this.sentEmails = new ArrayList<String>();
		for (int i = 0; i < newUser.getSentEmails().size(); i++) {
			this.sentEmails.add(newUser.getSentEmails().get(i));
		}
		
		this.receivedEmails = new ArrayList<String>();
		for (int i = 0; i < newUser.getReceivedEmails().size(); i++) {
			this.receivedEmails.add(newUser.getReceivedEmails().get(i));
		}
		
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the sentEmails
	 */
	public ArrayList<String> getSentEmails() {
		return sentEmails;
	}
	/**
	 * @param sentEmails the sentEmails to set
	 */
	public void setSentEmails(ArrayList<String> sentEmails) {
		this.sentEmails = sentEmails;
	}
	/**
	 * @return the receivedEmails
	 */
	public ArrayList<String> getReceivedEmails() {
		return receivedEmails;
	}
	/**
	 * @param receivedEmails the receivedEmails to set
	 */
	public void setReceivedEmails(ArrayList<String> receivedEmails) {
		this.receivedEmails = receivedEmails;
	}
	
}
