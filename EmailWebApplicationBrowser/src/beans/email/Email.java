package beans.email;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Email class for creating email instances
 * @author Jelena Banjac
 *
 */
@XmlRootElement
public class Email implements Serializable {

	/**
	 * Email class version
	 */
	private static final long serialVersionUID = -7632866623548155618L;
	
	/**
	 * Email identification string value
	 */
	private String emailId;
	/**
	 * Username of the user that receives the email
	 */
	private String toUser;
	/**
	 * Username of the user that sends the email
	 */
	private String fromUser;
	/**
	 * Email subject
	 */
	private String subject;
	/**
	 * Content of the email
	 */
	private String content;
	
	/**
	 * Default email constructor
	 */
	public Email() {}
	
	/**
	 * Email constructor with following parameters
	 * @param emailId
	 * @param toUser
	 * @param fromUser
	 * @param subject
	 * @param content
	 */
	public Email(String emailId, String toUser, String fromUser, String subject, String content) {
		super();
		this.emailId = emailId;
		this.toUser = toUser;
		this.fromUser = fromUser;
		this.subject = subject;
		this.content = content;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/**
	 * @return the toUser
	 */
	public String getToUser() {
		return toUser;
	}
	/**
	 * @param toUser the toUser to set
	 */
	public void setToUser(String toUser) {
		this.toUser = toUser;
	}
	/**
	 * @return the fromUser
	 */
	public String getFromUser() {
		return fromUser;
	}
	/**
	 * @param fromUser the fromUser to set
	 */
	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Email [emailId=" + emailId + ", toUser=" + toUser + ", fromUser=" + fromUser + ", subject=" + subject
				+ ", content=" + content + "]";
	}
	
	
}
