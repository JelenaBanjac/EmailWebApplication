/**
 * 
 */
package servlets.misc;

import java.util.ArrayList;

/**
 * @author Jelena Banjac
 *
 */
public class UtilsData {
	/**
	 * List of data that will be transported from server
	 * to the client.
	 */
	public static ArrayList<EmailData> transportData = new ArrayList<EmailData>();
	
	/**
	 * Finds EmailData using usename, that identifies
	 * instances of data transported.
	 * @param username - client's username
	 * @return emailData - data transfered for the user, otherwise null
	 * (user haven't have any transportations yet)
	 */
	public static EmailData findEmailDataByUsername(String username) {
		
		for (int i = 0; i < transportData.size(); i++) {
			
			if (transportData.get(i).getUsername().equals(username)) {
				
				return transportData.get(i);
			}
		}
		return null;
	}
}
