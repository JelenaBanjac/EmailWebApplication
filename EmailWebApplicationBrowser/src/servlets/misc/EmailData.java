package servlets.misc;

import java.io.BufferedReader;
import java.io.PrintWriter;

/**
 * Buffer for messages that are sent to server
 * @author Jelena Banjac
 *
 */
public class EmailData {
	/**
	 * Message that will be received by client
	 */
	private String message;
	/**
	 * Username of the client that receives server 
	 * response
	 */
	private String username;
	/**
	 * Reader that reads messages got from the server
	 */
	private BufferedReader in;
	/**
	 * Printer that writes messages to server
	 */
	private PrintWriter out;
	
	/**
	 * Default constructor for data exchanged between
	 * server and browser client
	 * @param username - client's username
	 * @param in - input stream (from server)
	 * @param out - output steram (to server)
	 */
	public EmailData(String username, BufferedReader in, PrintWriter out) {
		this.username = username;
		this.in = in;
		this.out = out;
	}
	

	/**
	 * Get message that will be received by client, but first wait
	 * @return the messageReceivedString
	 */
	public synchronized String getReceiveMessage() {
		try {
			wait();
		} catch (Exception ex) { }
		
		return message;
	}

	/**
	 * Set message that will be received and notify thread
	 * @param messageReceivedString the messageReceivedString to set
	 */
	public synchronized void setReceiveMessage(String message) {
		this.message = message;
		notify();
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the in
	 */
	public BufferedReader getIn() {
		return in;
	}

	/**
	 * @param in the in to set
	 */
	public void setIn(BufferedReader in) {
		this.in = in;
	}

	/**
	 * @return the out
	 */
	public PrintWriter getOut() {
		return out;
	}

	/**
	 * @param out the out to set
	 */
	public void setOut(PrintWriter out) {
		this.out = out;
	}

	
}
