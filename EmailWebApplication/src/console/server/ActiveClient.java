package console.server;

import java.util.ArrayList;

/**
 * Represents one active client (similar to the user class)
 * This class we use to take care of loggedin users
 * @author Jelena Banjac
 *
 */
public class ActiveClient {
	/**
	 * User username
	 */
	private String username;
	/**
	 * User address
	 */
	private String address;
	/**
	 * User console message (text that is typed)
	 */
	private String message;
	/**
	 * Emails that are sent by user
	 */
	private ArrayList<String> sentEmails;
	/**
	 * Emails that user received
	 */
	private ArrayList<String> receivedEmails;
	
	/**
	 * Default user constructor
	 */
	public ActiveClient() {}
	
	/**
	 * Constructor with the following parameters
	 * @param username
	 * @param address
	 */
	public ActiveClient(String username, String address) {
		this.username = username;
		this.address = address;
		this.message = "";
		this.sentEmails = new ArrayList<String>();
		this.receivedEmails = new ArrayList<String>();
	}
	
	/**
	 * Constructor with the following parameters
	 * @param username
	 * @param address
	 * @param sentEmails
	 * @param receivedEmails
	 */
	public ActiveClient(String username, String address, ArrayList<String> sentEmails, ArrayList<String> receivedEmails) {
		this.username = username;
		this.address = address;
		this.message = "";
		this.sentEmails = sentEmails;
		this.receivedEmails = receivedEmails;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Notify the thread when the message is set
	 * @param message
	 */
	public synchronized void setMessage(String message) {
		this.message = message;
		notify();
	}

	/**
	 * Wait till you get the message
	 * @return
	 */
	public synchronized String getMessage() {
		try {
			wait();
		} catch (Exception ex) {
		}
		return message;
	}

	/**
	 * @return the sentEmails
	 */
	public ArrayList<String> getSentEmails() {
		return sentEmails;
	}

	/**
	 * @param sentEmails the sentEmails to set
	 */
	public void setSentEmails(ArrayList<String> sentEmails) {
		this.sentEmails = sentEmails;
	}

	/**
	 * @return the receivedEmails
	 */
	public ArrayList<String> getReceivedEmails() {
		return receivedEmails;
	}

	/**
	 * @param receivedEmails the receivedEmails to set
	 */
	public void setReceivedEmails(ArrayList<String> receivedEmails) {
		this.receivedEmails = receivedEmails;
	}
	
	
	
}