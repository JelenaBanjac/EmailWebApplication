package console.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;

import beans.email.Email;
import beans.user.User;
import dao.email.EmailDao;
import dao.user.UserDao;

/**
 * Implementing the operations on active clients
 * @author Jelena Banjac
 *
 */
public class ClientUtils {

	/**
	 * Loggedin clients
	 */
	private static HashMap<String, ActiveClient> clients = new HashMap<String, ActiveClient>();
	/**
	 * Initializing the object of the class that 
	 * works with files with users inside
	 */
	private static UserDao userDao = new UserDao();
	/**
	 * Initializing the object of the class that 
	 * works with files with emails inside
	 */
	private static EmailDao emailDao = new EmailDao();
	
	public static ActiveClient getActiveClient(String username) {
		for (ActiveClient ac : clients.values()) {
			if (ac.getUsername().equals(username))
				return ac;
		}
		return null;
	}
	
	/**
	 * Add new logged in client
	 * @param username - client's username
	 * @param address - client's address
	 * @return added client, otherwise null
	 */
	public static synchronized ActiveClient addClient(String username, String address) {
		ActiveClient test = (ActiveClient) clients.get(username);
		if (test == null) {
			ActiveClient client = new ActiveClient(username, address, 
					userDao.findUserByUsername(username).getSentEmails(),
					userDao.findUserByUsername(username).getReceivedEmails());
			/*client.setReceivedEmails(userDao.findUserByUsername(username).getReceivedEmails());
			client.setSentEmails(userDao.findUserByUsername(username).getSentEmails());*/
			clients.put(username, client);
			return client;
		} else
			return null;
	}

	/**
	 * Removes the client from the list of loggedin clients
	 * @param username - username of the client that will be removed
	 * @return true - successfully removed, false - otherwise
	 */
	public static synchronized boolean removeClient(String username) {
		ActiveClient test = (ActiveClient) clients.get(username);
		if (test == null)
			return false;
		else
			clients.remove(username);
		return true;
	}

	/**
	 * Current client is sending an email to a receiver. 
	 * Receiver does not have to be logged in to receive an email,
	 * only sender needs to be logged in.
	 * @param sender - current client that send an email
	 * @param message - text that needs to be parsed in order to see 
	 * 					receiver, subject and content of the email
	 */
	public static void sendMessageToClient(String sender, String message) {
		/* Parse email information */
		String retVal = "";
		StringTokenizer st = new StringTokenizer(message,":");
		st.nextToken();
		String receiver = st.nextToken();
		receiver = receiver.substring(0, receiver.length() - 7).trim();
		String subject = st.nextToken();
		subject = subject.substring(0, subject.length() - 4).trim();
		String content = st.nextToken().trim();
		
		/* Create new mail object that will be sent */
		Email newEmail = new Email(emailDao.getEmailIdCounter(), receiver, sender, subject, content);
		
		/* Work with sender, receiver and email. Save every change made in the list of objects */
		User userSender = userDao.findUserByUsername(sender);
		User userReceiver = userDao.findUserByUsername(receiver);
		if (userSender != null && userReceiver != null) {
			/* RECEIVER */
			for (ActiveClient ac : clients.values()) {
				if (ac.getUsername().equals(receiver)) {
					ArrayList<String> receivedEmails = ac.getReceivedEmails();
					receivedEmails.add(newEmail.getEmailId());
					ac.setReceivedEmails(receivedEmails);
					break;
				} 
			}
			ArrayList<String> receivedEmails = userReceiver.getReceivedEmails();
			receivedEmails.add(newEmail.getEmailId());
			userReceiver.setReceivedEmails(receivedEmails);
			userDao.updateUser(receiver,userReceiver);
			
			/* SENDER */
			for (ActiveClient ac : clients.values()) {
				if (ac.getUsername().equals(sender)) {
					ArrayList<String> sentEmails = ac.getSentEmails();
					sentEmails.add(newEmail.getEmailId());
					ac.setSentEmails(sentEmails);
					
					userSender.setSentEmails(ac.getSentEmails());
					userDao.updateUser(sender, userSender);
					emailDao.addEmail(newEmail);
					break;
				} 
			}
			retVal += "Email successfully sent.\n";
		} else {
			retVal += "Receiver <"+receiver+"> does not exist!\n";
		}
				
		for (ActiveClient ac : clients.values()) {
			if (ac.getUsername().equals(sender))
				ac.setMessage(retVal);
		}
		
		System.out.println("User <" + sender + "> sent email to user <" + receiver + ">");	
	}
	
	/**
	 * Find received email by its ID
	 * @param username - current client username
	 * @param message - message written in client's terminal,
	 * 					template is RECEIVED <emailId>
	 */
	public static void findReceivedEmailById(String username, String message) {
		String emailId = message.substring(8);
		
		String retVal = "----------------------------\n";
		
		for (ActiveClient ac : clients.values()) {

			if (ac.getUsername().equals(username)) {	

				if (ac.getReceivedEmails() != null) {
	
					if (ac.getReceivedEmails().size() != 0) {
						int i = 0;
						for (i = 0; i < ac.getReceivedEmails().size(); i++) {
							if (ac.getReceivedEmails().get(i).equals(emailId)) {
								Email email = emailDao.findEmailById(emailId);
								retVal += "Id: " + email.getEmailId() + "\n";
								retVal += "From: " + userDao.findUserByUsername(email.getFromUser()).getUsername() + "\n";
								retVal += "To: " + userDao.findUserByUsername(email.getToUser()).getUsername() + "\n";
								retVal += "Subject: " + email.getSubject() + "\n";
								retVal += "Content: " + email.getContent() + "\n";
								retVal += "----------------------------\n";
								break;
							}
							
						}
						if (i == ac.getReceivedEmails().size()) {
							retVal += "No received email with email ID " + emailId + "\n";
							retVal += "----------------------------\n";
						}
					} else {
						retVal += "No received emails for you :(\n";
						retVal += "----------------------------\n";
					}
				}
				
			}		
		}
		
		for (ActiveClient ac : clients.values()) {
			if (ac.getUsername().equals(username))
				ac.setMessage(retVal);
		}
		
		System.out.println("Found received email by ID for the user <" + username + ">");
	
	}
	
	/**
	 * Lists all received emails of the user
	 * @param username - users's username
	 */
	public static void listReceivedEmails(String username) {
		String retVal = "----------------------------\n";
		
		for (ActiveClient ac : clients.values()) {

			if (ac.getUsername().equals(username)) {	

				if (ac.getReceivedEmails() != null) {
	
					if (ac.getReceivedEmails().size() != 0) {
						for (int i = 0; i < ac.getReceivedEmails().size(); i++) {
							Email email = emailDao.findEmailById(ac.getReceivedEmails().get(i));
							retVal += "Id: " + email.getEmailId() + "\n";
							retVal += "From: " + userDao.findUserByUsername(email.getFromUser()).getUsername() + "\n";
							retVal += "To: " + userDao.findUserByUsername(email.getToUser()).getUsername() + "\n";
							retVal += "Subject: " + email.getSubject() + "\n";
							retVal += "Content: " + email.getContent() + "\n";
							retVal += "----------------------------\n";
							
						}
					} else {
						retVal += "No received emails for you :(\n";
						retVal += "----------------------------\n";
					}
				}
				
			}		
		}
		
		for (ActiveClient ac : clients.values()) {
			if (ac.getUsername().equals(username))
				ac.setMessage(retVal);
		}
		
		System.out.println("List all received emails for the user <" + username + ">");
	}
	
	/**
	 * Prints out on client's terminal to notify them about 
	 * command not found warning
	 * @param username - user's username
	 * @param message - text that client typed into their terminal
	 */
	public static void commandNotFound(String username, String message) {
		for (ActiveClient ac : clients.values()) {
			if (ac.getUsername().equals(username))
				ac.setMessage(message + ": Command not found\n");
		}
	}

	/**
	 * Gets the logged in users into server terminal
	 * @return collection of clients logged in to application
	 */
	public static Collection<ActiveClient> getClientList() {
		return clients.values();	
	}
	
	/**
	 * Prints the logged in users into server terminal
	 */
	public static void printClientList() {
		System.out.println("Logged in users:");
		for (ActiveClient ac : clients.values()) {
			System.out.println(ac.getUsername());
		}
	}

	
}