package console.server;

import java.io.BufferedReader;
import java.net.Socket;

/**
 * Thread for receiving messages from the client
 * @author Jelena Banjac
 *
 */
public class ReaderThread extends Thread {

	/**
	 * Declaring socket for client server communication channel
	 */
	private Socket sock;
	/**
	 * Input data that comes from the client
	 */
	private BufferedReader in;
	/**
	 * Declaring the client that is communicating with server
	 */
	private ActiveClient client;
	
	/**
	 * Constructor for reader thread
	 * @param sock - socket
	 * @param in -  input data that comes from client
	 * @param client - client that communicates with server
	 */
	public ReaderThread(Socket sock, BufferedReader in, ActiveClient client) {
		this.sock = sock;
		this.in = in;
		this.client = client;
		start();
	}

	/**
	 * When start() is called from the constructor, this run() is executed
	 */
	public void run() {
		try {
			System.out.println();
			String msg;
			//TODO ovde parsiram klijenta iz msg!
			while ((in != null) && ((msg = in.readLine())!= null) && !msg.equals("QUIT!")) {
				if (msg != null) {
					System.out.println("Server ReaderThred procesuira komandu "+msg);
					if (msg.startsWith("LOGOFF")) {
						System.out.println("Logging off user <" + client.getUsername() + ">");
						ClientUtils.removeClient(client.getUsername());
						break;
					} else if (msg.startsWith("LIST")) {
						ClientUtils.listReceivedEmails(client.getUsername());
					} else if (msg.startsWith("RECEIVE")) {
						ClientUtils.findReceivedEmailById(client.getUsername(), msg);
					} else if (msg.startsWith("SEND")) {
						ClientUtils.sendMessageToClient(client.getUsername(), msg);
					} else {
						ClientUtils.commandNotFound(client.getUsername(), msg);
					}
				} 				
			}

			in.close();
			sock.close();
			ClientUtils.removeClient(client.getUsername());
			ClientUtils.printClientList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	
}
