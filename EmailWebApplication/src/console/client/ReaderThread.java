package console.client;

import java.io.BufferedReader;

/**
 * Thread for reading the messages from the server
 * @author Jelena Banjac
 *
 */
public class ReaderThread extends Thread {

	/**
	 * Input data that comes from the server
	 */
	private BufferedReader in;
	
	/**
	 * Constructor for reader thread
	 * @param in - input data that comes from server
	 */
	public ReaderThread(BufferedReader in) {
		this.in = in;
		start();
	}

	/**
	 * When start() is called from the constructor, 
	 * this run() is executed. 
	 * Here we override it.
	 */
	public void run() {
		try {
			String msg;
			while (true) {
				msg = in.readLine();
				if (msg != null) {
					if (!msg.trim().equals("")) {
						System.out.println(msg);
						System.out.print(">");
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	
}
