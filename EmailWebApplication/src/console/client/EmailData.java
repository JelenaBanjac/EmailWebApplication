package console.client;

/**
 * Buffer for messages that are sent to server
 * @author Jelena Banjac
 *
 */
public class EmailData {
	/**
	 * Message that will be sent to server
	 */
	private String message;

	/**
	 * Set message that will be sent and notify thread
	 * @param message
	 */
	public synchronized void setMessage(String message) {
		this.message = message;
		notify();
	}

	/**
	 * Get message that will be sent to server, but first wait
	 * @return message
	 */
	public synchronized String getMessage() {
		try {
			wait();
		} catch (Exception ex) { }
		return message;
	}

	
}
