package console.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.StringTokenizer;

/**
 * Default class for client.
 * Here we run our application clients
 * @author Jelena Banjac
 *
 */
public class EmailClient {
	
	/**
	 * TCP port number
	 */
	public static final int TCP_PORT = 9000;
	/**
	 * Declaring the buffer for messages that are sent to server
	 */
	EmailData cd;
	/**
	 * Client address
	 */
	private String address;

	/**
	 * Default constructor setting the domain
	 * @param args
	 */
	public EmailClient(String[] args) {
		if (args.length == 0)
			address = "localhost";
		else
			address = args[0];

	}

	/** 
	 * Sends the message to the server
	 * @param s - message
	 */
	public void sendMessage(String s) {
		String message = s.trim();
		cd.setMessage(message);
	}
	
	/**
	 * Print out the possible commands to the client terminal
	 */
	public static void printCommands() {		
		System.out.println("-----------------------------------------");
		System.out.println("Commands:");
		System.out.println("\tREGISTER <username>");
		System.out.println("\tLOGIN <username>");
		System.out.println("\tLOGOFF");
		System.out.println("\tLIST");
		System.out.println("\tRECEIVE <message number>");
		System.out.println("\tSEND TO: <to> SUBJECT: <subject> TEXT: <text>");
		System.out.println("\tQUIT");
		System.out.println("-----------------------------------------");
	}

	/**
	 * Deal with user registration
	 * @param command - text written by user in their terminal
	 * @return true - success, false - failure
	 */
	@SuppressWarnings("resource")
	public boolean register(String command) {
		try {
			InetAddress addr = InetAddress.getByName(address);
			Socket sock = new Socket(addr, TCP_PORT);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(
					sock.getInputStream()));
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new OutputStreamWriter(sock.getOutputStream())), true);

			out.println(command);
			
			String response = in.readLine();
			System.out.println(response);
			out.println(response);
			
			if (!response.startsWith("FAIL")) {
				return true;
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return false;
	}
	
	/**
	 * Deal with multiple user login
	 * @param command
	 * @return
	 */
	@SuppressWarnings("resource")
	public boolean login(String command) {
		try {
			InetAddress addr = InetAddress.getByName(address);
			Socket sock = new Socket(addr, TCP_PORT);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(
					sock.getInputStream()));
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new OutputStreamWriter(sock.getOutputStream())), true);

			out.println(command);
			
			String response = in.readLine();
			System.out.println(response);
			//out.println(response);
			
			if (!response.startsWith("FAIL")) {
				cd = new EmailData();
				new ReaderThread(in);
				new WriterThread(out, cd);
				
				StringTokenizer st = new StringTokenizer(response,"-");
				st.nextToken();
				System.out.println("Email Client [" + st.nextToken() + "]");
				return true;
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 * Loop for the rest of the commands when 
	 * user is already logged in
	 */
	public void mainLoop() {
		BufferedReader kIn = new BufferedReader(new InputStreamReader(System.in));

		String command;
		while (true) {
			System.out.print(">");
			try {
				command = kIn.readLine();											
			} catch (Exception ex) {
				ex.printStackTrace();
				return;
			}
			
			if (!command.equals("QUIT")) {
				if (command.equals("LOGOFF")) {
					sendMessage(command);
					break;
				} else {
					sendMessage(command);
				}
			} else {
				sendMessage("QUIT!");
				break;
			}
		}
	}

	/**
	 * Opens application dialog and user needs to be 
	 * previously registered into system, after, logged in
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		System.out.println("#########################################");
		System.out.println("###          CONSOLE EMAIL            ###");
		System.out.println("#########################################");
		printCommands();
		
		BufferedReader kIn = new BufferedReader(new InputStreamReader(System.in));
		
		while (true) {
			System.out.print(">>");
			String command = kIn.readLine();
			
			if (command.startsWith("REGISTER") || command.startsWith("LOGIN")) {
				
				EmailClient cc = new EmailClient(args);
				
				if (command.startsWith("REGISTER")) {	/* Registration */
					if (!cc.register(command))
						continue;
					
				} else {							/* Login */
					if (!cc.login(command))
						continue;
					else
						cc.mainLoop();
				}
				
			} else {
				System.out.println("You need to REGISTER or LOGIN in order to proceed");
			}			
		}		
	}

	
}