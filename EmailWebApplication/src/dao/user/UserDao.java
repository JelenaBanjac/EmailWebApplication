package dao.user;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import beans.user.User;

/**
 * Work with JSON files in order to get list of users
 * and work with it (add, modify,...)
 * @author Jelena Banjac
 *
 */
public class UserDao {

	/**
	 * List of all users in the system
	 */
	public static ArrayList<User> users;
	/**
	 * Path to the corresponding JSON file with users
	 */
	private static String filePath;
	/**
	 * Initializing GSON object (for reading and writing)
	 */
	private static Gson gson = new Gson();
	
	/**
	 * Default constructor for setting up the environment parameters
	 */
	public UserDao() {
		//TODO change the path for your computer
		filePath = "/home/jelena/workspace/WS-RTRK/EmailApplication/EmailWebApplication/EmailWebApplication/WebContent/users.json";
		users = new ArrayList<User>();
	}
	
	/**
	 * Constructor with the following parameters
	 * @param newFilePath
	 */
	public UserDao(String newFilePath) {
		filePath = newFilePath;
		users = new ArrayList<User>();
	}
	
	/**
	 * Return the list of all users (Read-only)
	 * @return users
	 */
	public ArrayList<User> getUsers() {
		try {
			readUserJSON();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return users;
	}
	
	/**
	 * Add new user object to the list (and to the JSON file)
	 * @param newUser
	 */
	public void addUser(User newUser) {
		try {
			readUserJSON();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		users.add(newUser);
		try {
			writeUserJSON();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Update user with given username with information from updatedUser object
	 * @param username
	 * @param updatedUser
	 */
	public void updateUser(String username, User updatedUser) {
		try {
			readUserJSON();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		int i = 0;
		while (i < users.size()) {
			if (users.get(i).getUsername().equals(username)) {
				users.set(i, updatedUser);
				break;
			}
			i++;
		}
		
		try {
			writeUserJSON();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Find user by username
	 * @param username
	 * @return found user, otherwise null
	 */
	public User findUserByUsername(String username) {
		try {
			readUserJSON();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		int i = 0;
		while (i < users.size()) {
			if (users.get(i).getUsername().equals(username)) {
				return users.get(i);
			}
			i++;
		}
		
		return null;
	}
	
	/**
	 * Read users from the JSON file using GSON library
	 * @throws IOException
	 */
	private static void readUserJSON() throws IOException {
		synchronized (users) {
			Reader fileReader = new FileReader(filePath);
			users = gson.fromJson(fileReader, new TypeToken<List<User>>(){}.getType());
			fileReader.close();
		}
	}
	
	/**
	 * Write users to the JSON file using GSON library
	 * @throws IOException
	 */
	private static void writeUserJSON() throws IOException  {
		synchronized (users) {
			Writer writer = new FileWriter(filePath);
		    gson.toJson(users, writer);
		    writer.close();
		}
	}
	
	
	
}
