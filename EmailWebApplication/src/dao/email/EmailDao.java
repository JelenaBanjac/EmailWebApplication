package dao.email;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import beans.email.Email;

/**
 * Work with JSON files in order to get list of emails
 * and work with it (add, modify,...)
 * @author Jelena Banjac
 *
 */
public class EmailDao {

	/**
	 * List of all emails in the system
	 */
	public static ArrayList<Email> emails;
	/**
	 * Path to the corresponding JSON file with emails
	 */
	private static String filePath;
	/**
	 * Initializing GSON object (for reading and writing)
	 */
	private static Gson gson = new Gson();
	
	/**
	 * Default constructor for setting up the environment parameters
	 */
	public EmailDao() {
		//TODO change the path for your computer
		filePath = "/home/jelena/workspace/WS-RTRK/EmailApplication/EmailWebApplication/EmailWebApplication/WebContent/emails.json";
		emails = new ArrayList<Email>();
	}
	
	/**
	 * Constructor with the following parameters:
	 * @param newFilePath
	 */
	public EmailDao(String newFilePath) {
		filePath = newFilePath;
		emails = new ArrayList<Email>();
	}
	
	/**
	 * Return the list of all emails (Read-only)
	 * @return emails
	 */
	public ArrayList<Email> getEmails() {
		try {
			readEmailJSON();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return emails;
	}
	
	/**
	 * Generating email identification values
	 * @return nextEmailId - generated ID for the next imported email object
	 */
	public String getEmailIdCounter() {
		String nextEmailId = "1";
		if (emails.size() != 0) {
			String lastEmailId = emails.get(emails.size()-1).getEmailId();
			nextEmailId = String.valueOf(Integer.parseInt(lastEmailId)+1);
			
		} 
		return nextEmailId;
	}
	
	/**
	 * Add new email object to the list (and to the JSON file)
	 * @param newEmail
	 */
	public void addEmail(Email newEmail) {
		try {
			readEmailJSON();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		emails.add(newEmail);
		
		try {
			writeEmailJSON();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Find email by ID
	 * @param emailId
	 * @return found email object, otherwise null
	 */
	public Email findEmailById(String emailId) {
		try {
			readEmailJSON();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		int i = 0;
		while (i < emails.size()) {
			if (emails.get(i).getEmailId().equals(emailId)) {
				return emails.get(i);
			}
			i++;
		}
		
		return null;
	}
	
	/**
	 * Read emails from the JSON file using GSON library
	 * @throws IOException
	 */
	private static void readEmailJSON() throws IOException {
		synchronized (emails) {
			Reader fileReader = new FileReader(filePath);
			emails = gson.fromJson(fileReader, new TypeToken<List<Email>>(){}.getType());
			fileReader.close();
		}
	}
	
	/**
	 * Write emails to the JSON file using GSON library
	 * @throws IOException
	 */
	private static void writeEmailJSON() throws IOException  {
		synchronized (emails) {
			Writer writer = new FileWriter(filePath);
		    gson.toJson(emails, writer);
		    writer.close();
		}
	}
	
}
