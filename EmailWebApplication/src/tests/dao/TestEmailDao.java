/**
 * 
 */
package tests.dao;


import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import beans.email.Email;
import beans.user.User;
import dao.email.EmailDao;
import dao.user.UserDao;
/**
 * @author Jelena Banjac
 *
 */
public class TestEmailDao {
	/**
	 * Declaration of the object that works with 
	 * files related to emails in the system
	 */
	private EmailDao emailDao;
	/**
	 * Declaration of the object that works with 
	 * files related to users in the system
	 */
	private UserDao userDao;
	/**
	 * Setting up the path to the file with test emails
	 */
	//TODO set the path for your computer
	private static String filePathEmail = "/home/jelena/workspace/WS-RTRK/EmailApplication/EmailWebApplication/EmailWebApplication/WebContent/emails-test.json";
	/**
	 * Setting up the path to the file with test users
	 */
	//TODO set the path for your computer
	private static String filePathUser = "/home/jelena/workspace/WS-RTRK/EmailApplication/EmailWebApplication/EmailWebApplication/WebContent/users-test.json";
	
	/**
	 * Set up the environment before the start of testing
	 * @throws Exception
	 */
	@Before 
	public void setUp() throws Exception {
		emailDao = new EmailDao(filePathEmail);
		userDao = new UserDao(filePathUser);
	}
	
	/**
	 * Test that checks writing to and reading from the JSON file of email objects
	 */
	@Test
	public void checkWriteReadJSON() {
		// Do we have entries for email?
		boolean createNewEntriesEmail = (emailDao.getEmails().size() == 0);
		
		// Do we have entries for user?
		boolean createNewEntriesUser = (userDao.getUsers().size() == 0);
		
		// No, so let's create new ones and write it (using userDao.addUser() command)
		if (createNewEntriesEmail) {
			assertTrue(emailDao.getEmails().size() == 0);
			
			if (createNewEntriesUser) {
				assertTrue(userDao.getUsers().size() == 0);
				
				for (int i = 0; i < 50; i++) {
					User newUser = new User();
					newUser.setUsername("Imenko_"+i);
					userDao.addUser(newUser);
				}
			}
			for (int i = 0; i < 20; i++) {
				Email newEmail = new Email();
				newEmail.setEmailId("Emailko_"+i);
				newEmail.setFromUser(userDao.findUserByUsername("Imenko_"+ (i)).getUsername());
				newEmail.setToUser(userDao.findUserByUsername("Imenko_"+ (i+1)).getUsername());
				emailDao.addEmail(newEmail);
				
				User fromUser = userDao.findUserByUsername("Imenko_"+ i);
				ArrayList<String> fromUserEmails = fromUser.getSentEmails();
				fromUserEmails.add(newEmail.getEmailId());
				fromUser.setSentEmails(fromUserEmails);
				userDao.updateUser(fromUser.getUsername(), fromUser);
				
				User toUser = userDao.findUserByUsername("Imenko_"+ (i+1));
				ArrayList<String> toUserEmails = toUser.getReceivedEmails();
				toUserEmails.add(newEmail.getEmailId());
				toUser.setSentEmails(toUserEmails);
				userDao.updateUser(toUser.getUsername(), toUser);
			}
			
		}
		
		// Read from file (inside userDao.getUsers() command)
		System.out.println("LIST --------------");
		for (int i = 0; i < emailDao.getEmails().size(); i++) {
			Email email = emailDao.getEmails().get(i);
			System.out.println(email.getEmailId());
		}
		System.out.println("-------------------");
		
		// Check the size again
		assertTrue(emailDao.getEmails().size() == 20);
		Email userEntity = emailDao.getEmails().get(0);
		assertTrue(userEntity.getEmailId().equals("Emailko_0"));
		assertTrue(userEntity.getToUser().equals("Imenko_1"));
		assertTrue(userEntity.getFromUser().equals("Imenko_0"));
		User fromUser = userDao.findUserByUsername("Imenko_0");
		assertTrue(fromUser.getSentEmails().size() == 1);
		User toUser = userDao.findUserByUsername("Imenko_1");
		assertTrue(toUser.getReceivedEmails().size() == 1);
	}
	
	/**
	 * Test that checks whether finding email by ID works appropriately 
	 */
	@Test
	public void checkFindingEmailById() {
		// Do we have entries for email?
		boolean createNewEntriesEmail = (emailDao.getEmails().size() == 0);
		
		// Do we have entries for user?
		boolean createNewEntriesUser = (userDao.getUsers().size() == 0);
		
		// No, so let's create new ones and write it (using userDao.addUser() command)
		if (createNewEntriesEmail) {
			assertTrue(emailDao.getEmails().size() == 0);
			
			if (createNewEntriesUser) {
				assertTrue(userDao.getUsers().size() == 0);
				
				for (int i = 0; i < 50; i++) {
					User newUser = new User();
					newUser.setUsername("Imenko_"+i);
					userDao.addUser(newUser);
				}
			}
			for (int i = 0; i < 20; i++) {
				Email newEmail = new Email();
				newEmail.setEmailId("Emailko_"+i);
				newEmail.setFromUser(userDao.findUserByUsername("Imenko_"+i).getUsername());
				newEmail.setToUser(userDao.findUserByUsername("Imenko_"+ (i+1)).getUsername());
				emailDao.addEmail(newEmail);
				
				User fromUser = userDao.findUserByUsername("Imenko_"+i);
				ArrayList<String> fromUserEmails = fromUser.getSentEmails();
				fromUserEmails.add(newEmail.getEmailId());
				fromUser.setSentEmails(fromUserEmails);
				userDao.updateUser(fromUser.getUsername(), fromUser);
				
				User toUser = userDao.findUserByUsername("Imenko_"+ (i+1));
				ArrayList<String> toUserEmails = toUser.getReceivedEmails();
				toUserEmails.add(newEmail.getEmailId());
				toUser.setSentEmails(toUserEmails);
				userDao.updateUser(toUser.getUsername(), toUser);
			}
			
		}
		
		// Find email with id Emailko_5
		Email emailEntity = emailDao.findEmailById("Emailko_5");	
		assertTrue(emailEntity.getEmailId().equals("Emailko_5"));
		assertTrue(emailEntity.getFromUser().equals("Imenko_5"));
		assertTrue(emailEntity.getToUser().equals("Imenko_6"));
	}
	
	
}
