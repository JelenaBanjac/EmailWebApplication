/**
 * 
 */
package tests.dao;


import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import beans.user.User;
import dao.user.UserDao;
/**
 * @author jelena
 *
 */
public class TestUserDao {
	/**
	 * Declaration of the object that works with 
	 * files related to users in the system
	 */
	private UserDao userDao;
	/**
	 * Setting up the path to the file with test users
	 */
	//TODO set the path for your computer
	private static String filePath = "/home/jelena/workspace/WS-RTRK/EmailApplication/EmailWebApplication/EmailWebApplication/WebContent/users-test.json";
	
	/**
	 * Set up the environment before the start of testing
	 * @throws Exception
	 */
	@Before 
	public void setUp() throws Exception {
		userDao = new UserDao(filePath);
	}
	
	/**
	 * Test that checks writing to and reading from the JSON file of user objects
	 */
	@Test
	public void checkWriteReadJSON() {
		// Do we have entries?
		boolean createNewEntries = (userDao.getUsers().size() == 0);
		
		// No, so let's create new ones and write it (using userDao.addUser() command)
		if (createNewEntries) {
			assertTrue(userDao.getUsers().size() == 0);
			
			for (int i = 0; i < 50; i++) {
				User newUser = new User();
				newUser.setUsername("Imenko_"+i);
				userDao.addUser(newUser);
			}
			
		}
		
		// Read from file (inside userDao.getUsers() command)
		System.out.println("LIST --------------");
		for (int i = 0; i < userDao.getUsers().size(); i++) {
			User user = userDao.getUsers().get(i);
			System.out.println(user.getUsername());
		}
		System.out.println("-------------------");
		
		// Check the size again
		assertTrue(userDao.getUsers().size() == 50);
		User userEntity = userDao.getUsers().get(0);
		assertTrue(userEntity.getUsername().equals("Imenko_0"));
		assertTrue(userEntity.getSentEmails().size() == 0);
		assertTrue(userEntity.getReceivedEmails().size() == 0);
	}
	
	/**
	 * Test that checks whether finding user by username works appropriately 
	 */
	@Test
	public void checkFindingUserById() {
		// Do we have entries?
		boolean createNewEntries = (userDao.getUsers().size() == 0);
		
		// No, so let's create new ones and write it (using userDao.addUser() command)
		if (createNewEntries) {
			assertTrue(userDao.getUsers().size() == 0);
			
			for (int i = 0; i < 50; i++) {
				User newUser = new User();
				newUser.setUsername("Imenko_"+i);
				userDao.addUser(newUser);
			}
			
		}
		
		// Find user with username Imenko_5
		User userEntity = userDao.findUserByUsername("Imenko_5");	
		assertTrue(userEntity.getUsername().equals("Imenko_5"));
		assertTrue(userEntity.getSentEmails().size() == 0);
		assertTrue(userEntity.getReceivedEmails().size() == 0);
	}
	
	/**
	 * Test that checks whether updating user works appropriately
	 */
	@Test
	public void checkUpdatingUser() {
		// Do we have entries?
		boolean createNewEntries = (userDao.getUsers().size() == 0);
		
		// No, so let's create new ones and write it (using userDao.addUser() command)
		if (createNewEntries) {
			assertTrue(userDao.getUsers().size() == 0);
			
			for (int i = 0; i < 50; i++) {
				User newUser = new User();
				newUser.setUsername("Imenko_"+i);
				userDao.addUser(newUser);
			}
			
		}
		
		// Find user with username Imenko_5
		User userEntity = userDao.findUserByUsername("Imenko_5");
		// Change his username (specifically, changing username is not used)
		userEntity.setUsername("Imenko_100");
		userDao.updateUser("Imenko_5", userEntity);
		assertTrue(userEntity.getUsername().equals("Imenko_100"));
		assertTrue(userEntity.getSentEmails().size() == 0);
		assertTrue(userEntity.getReceivedEmails().size() == 0);
	}
}
