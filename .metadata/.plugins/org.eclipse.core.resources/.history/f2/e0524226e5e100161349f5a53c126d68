/**
 * 
 */
package resources.user;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import beans.user.User;

/**
 * @author jelena
 *
 */
@Path("/users")
public class UserResource {

	UserDao userDao = new UserDao();
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<User> findAll() {
		System.out.println("Find all users");
		return userDao.findAll();
	}
	
	@GET @Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public User findByUsername(@PathParam("username") String username) {
		System.out.println("Find user by username (ID) " + username);
		return userDao.findByUsername(username);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public User create(User user) {
		System.out.println("Creating user");
		return userDao.create(user);
	}

	@PUT @Path("{username}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public User update(User user) {
		System.out.println("Updating user: " + user.getUsername());
		userDao.update(user);
		return user;
	}
	
	@DELETE @Path("{username}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void remove(@PathParam("username") String username) {
		userDao.remove(username);
	}
	
	
}
