package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import beans.email.Email;
import servlets.misc.EmailData;
import servlets.misc.ReaderThread;
import servlets.misc.UtilsData;

/**
 * Class that takes care of receiving all necessary data
 * stored on Email Server (previous project task).
 * e.g. list of users and e-mails
 * 
 * @author Jelena Banjac
 *
 */
public class DataDao {
	/**
	 * IP address of the host
	 */
	private InetAddress addr;
	/**
	 * Socket that will be opened from the client side
	 */
	private Socket sock;
	/**
	 * Response from sever (consol's server)
	 */
	private BufferedReader in;
	/**
	 * Request to server (consol's server)
	 */
	private PrintWriter out;
	/**
	 * TCP port number
	 */
	public static final int TCP_PORT = 9000;
	/**
	 * Declaring the buffer for messages that are sent to server
	 */
	EmailData cd;
	/**
	 * Host name
	 */
	private String address;
	/**
	 * Email Server's response represented as string
	 */
	private String stringServerResponse = "";
	/**
	 * Email Sever's response represented as email list
	 */
	private ArrayList<Email> listServerResponse = new ArrayList<Email>();
	
	/**
	 * Default DAO constructor
	 */
	public DataDao() {
		address = "localhost";
    	try {
    		addr = InetAddress.getByName(address);
    		sock = new Socket(addr, TCP_PORT);
    		
    		in = new BufferedReader(new InputStreamReader(
    				sock.getInputStream()));
    		out = new PrintWriter(new BufferedWriter(
    				new OutputStreamWriter(sock.getOutputStream())), true);
    	} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Close socket as well as his input and output streams
	 */
	public void closeSocket() {
		try {
			in.close();
			out.close();
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * When class of resources needs string sever response
	 * @return the stringServerResponse
	 */
	public String getStringServerResponse() {
		return stringServerResponse;
	}


	/**
	 * Set string server response when response is received from server
	 * @param stringServerResponse the stringServerResponse to set
	 */
	public void setStringServerResponse(String stringServerResponse) {
		this.stringServerResponse = stringServerResponse;
	}


	/**
	 * When class of resources needs list server response
	 * @return the listServerResponse
	 */
	public List<Email> getListServerResponse() {
		//System.out.println("uso");
		//System.out.println(listServerResponse.size());
		for (int i = 0; i < listServerResponse.size(); i++) {
			//System.out.println("\n$$$\nADD NEW EMAIL: \nId:"+listServerResponse.get(i).getEmailId()+"\nFrom:"+listServerResponse.get(i).getFromUser());
		}
		return listServerResponse;
	}

	/**
	 * Set list server response when response is received from server
	 * @param listServerResponse the listServerResponse to set
	 */
	public void setListServerResponse(ArrayList<Email> listServerResponse) {
		this.listServerResponse = listServerResponse;
	}

	/**
	 * Wait to get message from server as response,
	 * afterwards, process the response
	 */
	public void receiveMessage() {	
		//System.out.println("uso u receive msg: ");
		String receivedMessage = cd.getReceiveMessage();
		//System.out.println("dobio: "+receivedMessage);
		if (receivedMessage.startsWith("---")) {
			//System.out.println("parsira");
			parseReceivedList(receivedMessage);
		} else {
			this.stringServerResponse = receivedMessage;;
		}
	}
	
	/**
	 * If received message seems to be a list,
	 * parse that string into fitting list.
	 * @param message
	 */
	public void parseReceivedList(String message) {
		String[] emails = message.split("----------------------------\n");
		
		for (int i = 1; i < emails.length; i++) {
			Email emailObj = new Email();
			String[] email = emails[i].split("\n");
			
			if (email[0].startsWith("Id")) {
				emailObj.setEmailId(email[0].substring(4).trim());
			} else {
				this.stringServerResponse = email[0];
				break;
			}
			if (email[1].startsWith("From")) {
				emailObj.setFromUser(email[1].substring(6).trim());
			}
			if (email[2].startsWith("To")) {
				emailObj.setToUser(email[2].substring(4).trim());
			}
			if (email[3].startsWith("Subject")) {
				emailObj.setSubject(email[3].substring(9).trim());
			} else {
				emailObj.setSubject("/");
			}
			if (email[4].startsWith("Content")) {				
				emailObj.setContent(email[4].substring(9).trim());
			} else {
				emailObj.setContent("/");
			}
			this.listServerResponse.add(emailObj);
		}
	}
	
	/**
	 * Deal with user registration
	 * @param command - text written by user in their terminal
	 * @return true - success, false - failure
	 */
	public boolean register(String command) {
		try {
			
			out.println(command);
			
			String response = in.readLine();
			
			System.out.println(response);
			
			if (!response.startsWith("FAIL")) {
				
				stringServerResponse = response;
				
				return true;
			} else {
				stringServerResponse = response.substring(6);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		
		return false;
	}
	
	/**
	 * Deal with multiple user login
	 * @param command - special string that shows our needs towards server
	 * @return
	 */
	public boolean login(String command) {
		try {
			
			out.println(command);
			
			String response = in.readLine();
			
			System.out.println(response);
			
			if (!response.startsWith("FAIL")) {
				String username = command.substring(7).trim();
				
				cd = UtilsData.findEmailDataByUsername(username);
				if (cd == null) {
					cd = new EmailData(username, in, out);
					
					ReaderThread rt = new ReaderThread(in, cd);

					UtilsData.transportData.add(cd);
					
					stringServerResponse = response;
					
					rt.terminate();
					
					return true;
				} else {
					String[] tokens = command.split("~");
					mainLoop("~"+tokens[2]+"~LOGOFF");
					return true;
				}
				
				
			} else {
				stringServerResponse = response.substring(6);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		
		return false;
	}

	/**
	 * Loop for the rest of the commands when 
	 * user is already logged in or registered
	 * @param command - special string that shows our needs towards server
	 */
	public void mainLoop(String command) {
		String[] tokens = command.split("~");
		String username = tokens[1].trim();
		cd = UtilsData.findEmailDataByUsername(username);
		
		if (cd == null) {
			System.out.println("CD je NULL = try to log off then");
			cd = new EmailData(username, in, out);			
			UtilsData.transportData.add(cd);	
			tokens[2] = "LOGOFF";
		}
		
		if (!tokens[2].equals("QUIT")) {
			if (tokens[2].equals("LOGOFF")) { /* LOGOFF */
				
				System.out.println("LOGOFF -> "+command);
			
				//out.println(command);
				out.println("~"+username+"~LOGOFF");
				
				UtilsData.transportData.remove(cd);
				
			} else {	/* LIST, RECEIVE, SEND*/
				
				System.out.println("Sending command to sevrer -> "+command);
				
				out.println(command);
				
				ReaderThread rt = new ReaderThread(cd.getIn(), cd);
				
				receiveMessage();
			
				UtilsData.transportData.remove(cd);
				in = cd.getIn();
				cd = new EmailData(username, in, out);
				
				UtilsData.transportData.add(cd);
				rt.terminate();
				
				//System.out.println("...mainLoop() kraj...");
			}
		} else {	/* QUIT */
			out.println("QUIT!");
			
		}
		
	}
	
	
}
